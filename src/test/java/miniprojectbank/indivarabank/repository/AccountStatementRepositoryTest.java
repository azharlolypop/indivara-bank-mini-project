//package miniprojectbank.indivarabank.repository;
//
//import miniprojectbank.indivarabank.IndivaraBankApplication;
//import miniprojectbank.indivarabank.dto.mutation.TransactionDTO;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.mockito.Mock;
//import org.mockito.junit.jupiter.MockitoExtension;
//import org.springframework.boot.test.context.SpringBootTest;
//
//import java.util.List;
//
//import static org.junit.jupiter.api.Assertions.assertNotNull;
//
//@ExtendWith(MockitoExtension.class)
//@SpringBootTest(classes = IndivaraBankApplication.class)
//class AccountStatementRepositoryTest {
//
//    @Mock
//    private AccountStatementRepository accountStatementRepository;
//
//    @Test
//    void getDepositByValidAccount() {
//        Long accountId = 1L;
//
//        List<TransactionDTO> deposits = accountStatementRepository.getDepositsByAccountId(accountId);
//
//        assertNotNull(deposits);
//    }
//
//    @Test
//    void getWithdrawByValidAccount() {
//        Long accountId = 1L;
//
//        List<TransactionDTO> withdrawals = accountStatementRepository.getWithdrawalsByAccountId(accountId);
//
//        assertNotNull(withdrawals);
//    }
//
//    @Test
//    void getTransferByValidAccount() {
//        Long accountId = 1L;
//
//        List<TransactionDTO> transfers = accountStatementRepository.getTransfersByAccountId(accountId);
//
//        assertNotNull(transfers);
//    }
//}
//package miniprojectbank.indivarabank.repository;
//
//import miniprojectbank.indivarabank.IndivaraBankApplication;
//import miniprojectbank.indivarabank.entity.Account;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.mockito.Mock;
//import org.mockito.junit.jupiter.MockitoExtension;
//import org.springframework.boot.test.context.SpringBootTest;
//
//import java.util.Optional;
//
//import static org.junit.jupiter.api.Assertions.*;
//import static org.mockito.Mockito.when;
//
//@ExtendWith(MockitoExtension.class)
//@SpringBootTest(classes = IndivaraBankApplication.class)
//class AccountRepositoryTest {
//
//    @Mock
//    private AccountRepository accountRepository;
//
//    @Test
//    void testFindByAccountNumber() {
//        String accountNumber = "123456789";
//        Account expectedAccount = new Account();
//        expectedAccount.setAccountNumber(accountNumber);
//        when(accountRepository.findByAccountNumber(accountNumber)).thenReturn(Optional.of(expectedAccount));
//        Optional<Object> actualCustomer = accountRepository.findByAccountNumber(accountNumber);
//        assertEquals(expectedAccount, actualCustomer.orElseThrow());
//    }
//
//    @Test
//    void testFindByAccountNumber_NotFound() {
//        Optional<Object> foundCustomer = accountRepository.findByAccountNumber("987654321");
//        assertFalse(foundCustomer.isPresent());
//    }
//}
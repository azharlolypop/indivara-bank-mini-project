//package miniprojectbank.indivarabank.repository;
//
//import miniprojectbank.indivarabank.IndivaraBankApplication;
//import miniprojectbank.indivarabank.entity.Customers;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.mockito.Mock;
//import org.mockito.junit.jupiter.MockitoExtension;
//import org.springframework.boot.test.context.SpringBootTest;
//
//import java.util.Optional;
//
//import static org.junit.jupiter.api.Assertions.*;
//import static org.mockito.Mockito.when;
//
//@ExtendWith(MockitoExtension.class)
//@SpringBootTest(classes = IndivaraBankApplication.class)
//class CustomersRepositoryTest {
//
//    @Mock
//    private CustomersRepository customersRepository;
//
//    @Test
//    void testFindByUsername() {
//        String customer = "azharhaikall";
//        Customers customers = new Customers();
//        customers.setUsername(customer);
//        when(customersRepository.findByUsername(customer)).thenReturn(Optional.of(customers));
//        Optional<Customers> optionalUser = customersRepository.findByUsername(customer);
//        assertEquals(customers, optionalUser.orElse(null));
//    }
//
//    @Test
//    void testFindByUsername_NotFound() {
//        Optional<Customers> foundCustomer = customersRepository.findByUsername("nonexistentUser");
//        assertFalse(foundCustomer.isPresent());
//    }
//
//    @Test
//    void testExistsByUsername() {
//        String username = "azharhaikall";
//        when(customersRepository.existsByUsername(username)).thenReturn(true);
//        boolean exists = customersRepository.existsByUsername(username);
//        assertTrue(exists);
//    }
//}
package miniprojectbank.indivarabank.service;

import miniprojectbank.indivarabank.entity.Customers;
import miniprojectbank.indivarabank.repository.CustomersRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class TokenServiceTest {

    @Mock
    private CustomersRepository customersRepository;

    @InjectMocks
    private TokenService tokenService;

    @Test
    void testGetTokenValid() {
        String validToken = "validToken";
        Customers mockCustomer = new Customers();
        when(customersRepository.findByToken(validToken)).thenReturn(Optional.of(mockCustomer));

        boolean result = tokenService.getToken(validToken);

        assertTrue(result);
    }

    @Test
    void testGetTokenInvalid() {
        String invalidToken = "invalidToken";
        when(customersRepository.findByToken(invalidToken)).thenReturn(Optional.empty());

        boolean result = tokenService.getToken(invalidToken);

        assertFalse(result);
    }
}
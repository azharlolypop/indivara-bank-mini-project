package miniprojectbank.indivarabank.service;

import miniprojectbank.indivarabank.dto.transaction.TransferRequest;
import miniprojectbank.indivarabank.dto.transaction.TransferResponse;
import miniprojectbank.indivarabank.dto.custom.ApiResponseDataDTO;
import miniprojectbank.indivarabank.entity.Account;
import miniprojectbank.indivarabank.entity.Customers;
import miniprojectbank.indivarabank.entity.Transfer;
import miniprojectbank.indivarabank.exception.ErrorException;
import miniprojectbank.indivarabank.repository.AccountRepository;
import miniprojectbank.indivarabank.repository.TransferRepository;
import miniprojectbank.indivarabank.validation.TokenValidation;
import miniprojectbank.indivarabank.validation.TransferValidation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class TransferServiceTest {

    @Mock
    private TransferRepository transferRepository;

    @Mock
    private AccountRepository accountRepository;

    @Mock
    private TransferValidation transferValidation;

    @Mock
    private TokenValidation tokenValidation;

    @InjectMocks
    private TransferService transferService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        transferService = new TransferService(transferRepository, accountRepository, transferValidation, tokenValidation);
    }

    @Test
    void testTransfer() {
        String token = "321231";
        Long customerId = 1L;
        TransferRequest transferRequest = new TransferRequest();
        transferRequest.setPin("123456");
        transferRequest.setTransferAmount(new BigDecimal(1000));
        transferRequest.setAccountNumberDestination("123456789");

        Account account = new Account();
        account.setAccountId(customerId);
        account.setPin("123456");
        account.setTotalBalance(new BigDecimal(5000));

        // Pastikan account memiliki objek Customers yang terisi
        Customers sourceCustomer = new Customers();
        sourceCustomer.setUsername("sourceUser");
        account.setCustomer(sourceCustomer);

        Account destinationCustomer = new Account();
        destinationCustomer.setAccountId(2L);
        destinationCustomer.setTotalBalance(new BigDecimal(2000));

        // Pastikan destinationCustomer memiliki objek Customers yang terisi
        Customers destinationCustomerInfo = new Customers();
        destinationCustomerInfo.setUsername("destinationUser");
        destinationCustomer.setCustomer(destinationCustomerInfo);

        when(accountRepository.findById(customerId)).thenReturn(Optional.of(account));
        when(accountRepository.findByAccountNumber("123456789")).thenReturn(Optional.of(destinationCustomer));
        when(transferRepository.save(any())).thenAnswer(invocation -> {
            Transfer transfer = invocation.getArgument(0);
            transfer.setTransferId(1L);
            return transfer;
        });

        ApiResponseDataDTO response = transferService.transfer(token, customerId, transferRequest);

        assertTrue(response.getSuccess());
        assertNotNull(response.getData());
        assertTrue(response.getData() instanceof TransferResponse);

        TransferResponse transferResponse = (TransferResponse) response.getData();
        assertEquals(customerId, transferResponse.getAccountId());
        assertNotNull(transferResponse.getTransferId());
        assertEquals(transferRequest.getTransferAmount(), transferResponse.getTransferAmount());
        assertNotNull(transferResponse.getTransferDate());
        assertEquals(transferRequest.getAccountNumberDestination(), transferResponse.getAccountNumberDestination());

        // Verify that source customer's total balance is updated
        assertEquals(new BigDecimal(4000), account.getTotalBalance());

        // Verify that destination customer's total balance is updated
        assertEquals(new BigDecimal(3000), destinationCustomer.getTotalBalance());

        // Verify that transfer record is saved
        verify(transferRepository, times(1)).save(any());

        // Verify that username is set in TransferResponse
        assertNotNull(transferResponse.getUsername());
        assertEquals("destinationUser", transferResponse.getUsername());
    }

    @Test
    void testTransferSourceCustomerNotFound() {
        String token = "321231";
        Long accountId = 1L;
        TransferRequest transferRequest = new TransferRequest();
        transferRequest.setPin("123456");
        transferRequest.setTransferAmount(new BigDecimal(1000));
        transferRequest.setAccountNumberDestination("123456789");

        when(accountRepository.findById(accountId)).thenReturn(Optional.empty());

        assertThrows(ErrorException.class, () -> transferService.transfer(token, accountId, transferRequest));

        // Verify that transfer record is not saved when source customer not found
        verify(transferRepository, never()).save(any());
    }

    @Test
    void testTransferDestinationCustomerNotFound() {
        String token = "321231";
        Long accountId = 1L;
        TransferRequest transferRequest = new TransferRequest();
        transferRequest.setPin("123456");
        transferRequest.setTransferAmount(new BigDecimal(1000));
        transferRequest.setAccountNumberDestination("123456789");

        Account account = new Account();
        account.setAccountId(accountId);
        account.setPin("123456");
        account.setTotalBalance(new BigDecimal(5000));

        when(accountRepository.findById(accountId)).thenReturn(Optional.of(account));
        when(accountRepository.findByAccountNumber("123456789")).thenReturn(Optional.empty());

        assertThrows(ErrorException.class, () -> transferService.transfer(token, accountId, transferRequest));

        // Verify that transfer record is not saved when destination customer not found
        verify(transferRepository, never()).save(any());
    }

    @Test
    void testTransferValidationFailed() {
        String token = "321231";
        Long accountId = 1L;
        TransferRequest transferRequest = new TransferRequest();
        transferRequest.setPin("123456");
        transferRequest.setTransferAmount(new BigDecimal(1000));
        transferRequest.setAccountNumberDestination("123456789");

        Account account = new Account();
        account.setAccountId(accountId);
        account.setPin("456789");

        when(accountRepository.findById(accountId)).thenReturn(Optional.of(account));
        doThrow(new ErrorException(400, "Bad Request", "Invalid PIN")).when(transferValidation).validateTransferRequest(account, transferRequest);

        assertThrows(ErrorException.class, () -> transferService.transfer(token, accountId, transferRequest));

        // Verify that transfer record is not saved when validation fails
        verify(transferRepository, never()).save(any());
    }
}
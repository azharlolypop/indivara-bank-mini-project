package miniprojectbank.indivarabank.service;

import miniprojectbank.indivarabank.dto.transaction.WithdrawRequest;
import miniprojectbank.indivarabank.dto.transaction.WithdrawResponse;
import miniprojectbank.indivarabank.dto.custom.ApiResponseDataDTO;
import miniprojectbank.indivarabank.entity.Account;
import miniprojectbank.indivarabank.entity.Withdraw;
import miniprojectbank.indivarabank.exception.ErrorException;
import miniprojectbank.indivarabank.repository.AccountRepository;
import miniprojectbank.indivarabank.repository.WithdrawRepository;
import miniprojectbank.indivarabank.validation.TokenValidation;
import miniprojectbank.indivarabank.validation.WithdrawValidation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class WithdrawServiceTest {

    @Mock
    private WithdrawRepository withdrawRepository;

    @Mock
    private AccountRepository accountRepository;

    @Mock
    private WithdrawValidation withdrawValidation;

    @Mock
    private TokenValidation tokenValidation;

    @InjectMocks
    private WithdrawService withdrawService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        withdrawService = new WithdrawService(withdrawRepository, accountRepository, withdrawValidation, tokenValidation);
    }

    @Test
    void testWithdraw() {
        String token = "321231";
        Long accountId = 1L;
        WithdrawRequest withdrawRequest = new WithdrawRequest();
        withdrawRequest.setPin("123456");
        withdrawRequest.setWithdrawAmount(new BigDecimal(1000));

        Account account = new Account();
        account.setAccountId(accountId);
        account.setPin("123456");
        account.setTotalBalance(new BigDecimal(5000));

        when(accountRepository.findById(accountId)).thenReturn(Optional.of(account));
        when(withdrawRepository.save(any())).thenAnswer(invocation -> {
            Withdraw withdraw = invocation.getArgument(0);
            withdraw.setWithdrawId(1L);
            return withdraw;
        });

        ApiResponseDataDTO response = withdrawService.withdraw(token, accountId, withdrawRequest);

        assertTrue(response.getSuccess());
        assertNotNull(response.getData());
        assertTrue(response.getData() instanceof WithdrawResponse);

        WithdrawResponse withdrawResponse = (WithdrawResponse) response.getData();
        assertEquals(accountId, withdrawResponse.getAccountId());
        assertNotNull(withdrawResponse.getWithdrawId());
        assertEquals(withdrawRequest.getWithdrawAmount(), withdrawResponse.getWithdrawAmount());
        assertNotNull(withdrawResponse.getWithdrawDate());

        // Verify that customer's total balance is updated
        assertEquals(new BigDecimal(4000), account.getTotalBalance());

        // Verify that withdraw record is saved
        verify(withdrawRepository, times(1)).save(any());
    }

    @Test
    void testWithdrawCustomerNotFound() {
        String token = "321231";
        Long accountId = 1L;
        WithdrawRequest withdrawRequest = new WithdrawRequest();
        withdrawRequest.setPin("123456");
        withdrawRequest.setWithdrawAmount(new BigDecimal(1000));

        when(accountRepository.findById(accountId)).thenReturn(Optional.empty());

        assertThrows(ErrorException.class, () -> withdrawService.withdraw(token, accountId, withdrawRequest));

        // Verify that withdraw record is not saved when customer not found
        verify(withdrawRepository, never()).save(any());
    }

    @Test
    void testWithdrawValidationFailed() {
        String token = "321231";
        Long accountId = 1L;
        WithdrawRequest withdrawRequest = new WithdrawRequest();
        withdrawRequest.setPin("123456");
        withdrawRequest.setWithdrawAmount(new BigDecimal(1000));

        Account account = new Account();
        account.setAccountId(accountId);
        account.setPin("456789");

        when(accountRepository.findById(accountId)).thenReturn(Optional.of(account));
        doThrow(new ErrorException(400, "Bad Request", "Invalid PIN")).when(withdrawValidation).validateWithdrawRequest(account, withdrawRequest);

        assertThrows(ErrorException.class, () -> withdrawService.withdraw(token, accountId, withdrawRequest));

        // Verify that withdraw record is not saved when validation fails
        verify(withdrawRepository, never()).save(any());
    }
}
package miniprojectbank.indivarabank.service;

import com.lowagie.text.DocumentException;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletResponse;
import miniprojectbank.indivarabank.dto.mutation.TransactionDTO;
import miniprojectbank.indivarabank.entity.Account;
import miniprojectbank.indivarabank.entity.Customers;
import miniprojectbank.indivarabank.exception.ErrorException;
import miniprojectbank.indivarabank.repository.AccountRepository;
import miniprojectbank.indivarabank.repository.AccountStatementRepository;
import miniprojectbank.indivarabank.util.PdfGenerator;
import miniprojectbank.indivarabank.validation.TokenValidation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.mock.web.MockHttpServletResponse;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

class AccountStatementServiceTest {

    @Mock
    private AccountRepository accountRepository;

    @Mock
    private AccountStatementRepository accountStatementRepository;

    @Mock
    private TokenValidation tokenValidation;

    @Mock
    private PdfGenerator pdfGenerator;

    @InjectMocks
    private AccountStatementService accountStatementService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testGeneratePdfAccountStatementByAccountId() throws IOException, DocumentException {
        String token = "321231";
        Long accountId = 1L;
        HttpServletResponse response = mock(HttpServletResponse.class);
        ServletOutputStream servletOutputStream = mock(ServletOutputStream.class);

        // Mocking customer data
        Customers customer = new Customers();
        customer.setUsername("testUser");

        // Mocking account data
        Account account = new Account();
        account.setAccountId(accountId);
        account.setCustomer(customer);

        // Mocking transactions data
        List<TransactionDTO> deposits = Arrays.asList(new TransactionDTO(1L, "deposit", new BigDecimal("100.0"), LocalDateTime.now()));
        List<TransactionDTO> withdrawals = Arrays.asList(new TransactionDTO(2L, "withdraw", new BigDecimal("50.0"), LocalDateTime.now()));
        List<TransactionDTO> transfers = Arrays.asList(new TransactionDTO(3L, "transfer", new BigDecimal("30.0"), LocalDateTime.now()));

        assertNotNull(response);

        when(response.getOutputStream()).thenReturn(servletOutputStream);

        when(accountRepository.findById(accountId)).thenReturn(Optional.of(account));
        when(accountStatementRepository.getDepositsByAccountId(accountId)).thenReturn(deposits);
        when(accountStatementRepository.getWithdrawalsByAccountId(accountId)).thenReturn(withdrawals);
        when(accountStatementRepository.getTransfersByAccountId(accountId)).thenReturn(transfers);

        // Call the method to test
        accountStatementService.generatePdfAccountStatementByAccountId(token, accountId, response);

        verify(response).getOutputStream();
    }

    @Test
    void testGeneratePdfAccountStatementByAccountIdAccountNotFound() {
        String token = "321231";
        Long accountId = 1L;

        when(accountRepository.findById(accountId)).thenReturn(Optional.empty());

        // Mocking response
        MockHttpServletResponse response = new MockHttpServletResponse();

        // Call the method to test and expect an ErrorException
        ErrorException exception = assertThrows(ErrorException.class, () -> accountStatementService.generatePdfAccountStatementByAccountId(token, accountId, response));

        // Verify that ErrorException is thrown with the expected details
        assertThrows(ErrorException.class, () -> accountStatementService.generatePdfAccountStatementByAccountId(token, accountId, response));
    }
}
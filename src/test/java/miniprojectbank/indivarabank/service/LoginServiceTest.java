package miniprojectbank.indivarabank.service;

import miniprojectbank.indivarabank.dto.LoginRequest;
import miniprojectbank.indivarabank.dto.LoginResponse;
import miniprojectbank.indivarabank.dto.custom.ApiResponseDataDTO;
import miniprojectbank.indivarabank.entity.Customers;
import miniprojectbank.indivarabank.exception.ErrorException;
import miniprojectbank.indivarabank.repository.CustomersRepository;
import miniprojectbank.indivarabank.validation.LoginValidation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class LoginServiceTest {

    @Mock
    private CustomersRepository customersRepository;

    @Mock
    private LoginValidation loginValidation;

    @InjectMocks
    private LoginService loginService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testLoginCustomers() {
        Customers mockCustomer = mock(Customers.class);
        CustomersRepository mockCustomersRepository = mock(CustomersRepository.class);
        LoginValidation mockLoginValidation = mock(LoginValidation.class);

        LoginRequest request = new LoginRequest();
        request.setUsername("zhar.loly");
        request.setPassword("invalid_password");
        when(mockLoginValidation.validateLogin(request)).thenReturn(mockCustomer);
        when(mockCustomersRepository.save(mockCustomer)).thenReturn(mockCustomer);

        LoginService loginService = new LoginService(mockCustomersRepository, mockLoginValidation);

        ApiResponseDataDTO response = loginService.loginCustomers(request);

        assertTrue(response.getSuccess());
        assertNotNull(response.getData());
        assertTrue(response.getData() instanceof LoginResponse);
        LoginResponse loginResponse = (LoginResponse) response.getData();
        assertNotNull(loginResponse.getToken());
    }

    @Test
    void testLoginCustomersValidationFailed() {
        LoginRequest request = new LoginRequest();
        request.setUsername("zhar.loly");
        request.setPassword("invalid_password");

        when(loginValidation.validateLogin(request)).thenThrow(new ErrorException(401, "Unauthorized", "Validation failed"));

        assertThrows(RuntimeException.class, () -> loginService.loginCustomers(request));
        verify(customersRepository, never()).save(any(Customers.class));
    }
}

package miniprojectbank.indivarabank.service;

import miniprojectbank.indivarabank.dto.transaction.DepositRequest;
import miniprojectbank.indivarabank.dto.transaction.DepositResponse;
import miniprojectbank.indivarabank.dto.custom.ApiResponseDataDTO;
import miniprojectbank.indivarabank.entity.Account;
import miniprojectbank.indivarabank.entity.Deposit;
import miniprojectbank.indivarabank.exception.ErrorException;
import miniprojectbank.indivarabank.repository.AccountRepository;
import miniprojectbank.indivarabank.repository.DepositRepository;
import miniprojectbank.indivarabank.validation.DepositValidation;
import miniprojectbank.indivarabank.validation.TokenValidation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class DepositServiceTest {

    @Mock
    private DepositRepository depositRepository;

    @Mock
    private AccountRepository accountRepository;

    @Mock
    private DepositValidation depositValidation;

    @Mock
    private TokenValidation tokenValidation;

    @InjectMocks
    private DepositService depositService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testDeposit() {
        String token = "321231";
        Long accountId = 1L;
        DepositRequest depositRequest = new DepositRequest();
        depositRequest.setPin("123456");
        depositRequest.setDepositAmount(new BigDecimal(1000));

        Account account = new Account();
        account.setAccountId(accountId);
        account.setPin("123456");
        account.setTotalBalance(new BigDecimal(5000));

        when(accountRepository.findById(accountId)).thenReturn(Optional.of(account));
        when(depositRepository.save(any())).thenAnswer(invocation -> {
            Deposit deposit = invocation.getArgument(0);
            deposit.setDepositId(1L);
            return deposit;
        });

        ApiResponseDataDTO response = depositService.deposit(token, accountId, depositRequest);

        assertTrue(response.getSuccess());
        assertNotNull(response.getData());
        assertTrue(response.getData() instanceof DepositResponse);

        DepositResponse depositResponse = (DepositResponse) response.getData();
        assertEquals(accountId, depositResponse.getDepositId());
        assertNotNull(depositResponse.getDepositId());
        assertEquals(depositRequest.getDepositAmount(), depositResponse.getDepositAmount());
        assertNotNull(depositResponse.getDepositDate());

        // Verify total balance account is updated
        assertEquals(new BigDecimal(6000), account.getTotalBalance());

        // Verify that deposit record is saved
        verify(depositRepository, times(1)).save(any());
    }

    @Test
    void testDepositCustomerNotFound() {
        String token = "321231";
        Long accountId = 1L;
        DepositRequest depositRequest = new DepositRequest();
        depositRequest.setPin("123456");
        depositRequest.setDepositAmount(new BigDecimal(1000));

        when(accountRepository.findById(accountId)).thenReturn(Optional.empty());

        assertThrows(ErrorException.class, () -> depositService.deposit(token, accountId, depositRequest));

        // Verify deposit isn't saved when customer not found
        verify(depositRepository, never()).save(any());
    }

    @Test
    void testDepositValidationFailed() {
        String token = "321231";
        Long accountId = 1L;
        DepositRequest request = new DepositRequest();
        request.setPin("123456");
        request.setDepositAmount(new BigDecimal(1000));

        Account account = new Account();
        account.setAccountId(accountId);
        account.setPin("456789");

        when(accountRepository.findById(accountId)).thenReturn(Optional.of(account));
        doThrow(new ErrorException(400, "Bad Request", "Invalid PIN")).when(depositValidation).validateDepositRequest(account, request);

        assertThrows(ErrorException.class, () -> depositService.deposit(token, accountId, request));

        // Verify deposit isn't saved when validation failed
        verify(depositRepository, never()).save(any());
    }
}
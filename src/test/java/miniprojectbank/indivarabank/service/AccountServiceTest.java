package miniprojectbank.indivarabank.service;

import miniprojectbank.indivarabank.dto.AccountRequest;
import miniprojectbank.indivarabank.dto.AccountResponse;
import miniprojectbank.indivarabank.dto.custom.ApiResponseDataDTO;
import miniprojectbank.indivarabank.entity.Account;
import miniprojectbank.indivarabank.entity.Customers;
import miniprojectbank.indivarabank.exception.ErrorException;
import miniprojectbank.indivarabank.repository.AccountRepository;
import miniprojectbank.indivarabank.repository.CustomersRepository;
import miniprojectbank.indivarabank.validation.AccountValidation;
import miniprojectbank.indivarabank.validation.TokenValidation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.math.BigDecimal;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class AccountServiceTest {

    @Mock
    private AccountRepository accountRepository;

    @Mock
    private CustomersRepository customerRepository;

    @Mock
    private BCryptPasswordEncoder passwordEncoder;

    @Mock
    private AccountValidation accountValidation;

    @Mock
    private TokenValidation tokenValidation;

    @InjectMocks
    private AccountService accountService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void createAccount_Success() {
        // Arrange
        AccountRequest request = new AccountRequest();
        request.setFirstName("Azhar");
        request.setLastName("Haikal");
        request.setUsername("azharhaikall");
        request.setPassword("password");
        request.setDepositAmount(new BigDecimal("1000.00"));

        Customers savedCustomer = new Customers();
        savedCustomer.setCustomerId(1L);
        when(customerRepository.save(any())).thenReturn(savedCustomer);

        Account savedAccount = new Account();
        savedAccount.setAccountId(1L);
        savedAccount.setTotalBalance(new BigDecimal("1000.00"));
        when(accountRepository.save(any())).thenReturn(savedAccount);

        // Act
        ApiResponseDataDTO result = accountService.createAccount(request);

        // Assert
        assertTrue(result.getSuccess());
        assertNotNull(result.getData());
        assertTrue(result.getData() instanceof AccountResponse);
        AccountResponse response = (AccountResponse) result.getData();
        assertEquals("azharhaikall", response.getUsername());
    }

    @Test
    void createSecondAccount_Success() {
        // Arrange
        String token = "testtoken";
        Long customerId = 1L;
        AccountRequest request = new AccountRequest();
        request.setPin("1234");
        request.setDepositAmount(new BigDecimal("500.00"));

        Customers existingCustomer = new Customers();
        existingCustomer.setCustomerId(customerId);
        when(customerRepository.findById(customerId)).thenReturn(Optional.of(existingCustomer));

        Account savedAccount = new Account();
        savedAccount.setAccountId(2L);
        savedAccount.setTotalBalance(new BigDecimal("500.00"));
        when(accountRepository.save(any())).thenReturn(savedAccount);

        // Act
        ApiResponseDataDTO result = accountService.createSecondAccount(token, customerId, request);

        // Assert
        assertTrue(result.getSuccess());
        assertNotNull(result.getData());
        assertTrue(result.getData() instanceof AccountResponse);
    }

    @Test
    void createSecondAccount_CustomerNotFound() {
        // Arrange
        String token = "testtoken";
        Long customerId = 1L;
        AccountRequest request = new AccountRequest();

        when(customerRepository.findById(customerId)).thenReturn(Optional.empty());

        assertThrows(ErrorException.class, () -> accountService.createSecondAccount(token, customerId, request));

        verify(accountRepository, never()).save(any());
    }

    @Test
    void getAccountById_Success() {
        // Arrange
        String token = "testtoken";
        Long accountId = 1L;

        Customers customer = new Customers();
        customer.setCustomerId(1L);
        customer.setFirstName("Azhar");
        customer.setLastName("Haikal");
        customer.setPhone("123456789");

        Account existingAccount = new Account();
        existingAccount.setAccountId(accountId);
        existingAccount.setCustomer(customer);
        existingAccount.setTotalBalance(new BigDecimal("1000.00"));
        when(accountRepository.findById(accountId)).thenReturn(Optional.of(existingAccount));

        // Act
        ApiResponseDataDTO result = accountService.getAccountById(token, accountId);

        // Assert
        assertTrue(result.getSuccess());
        assertNotNull(result.getData());
        assertTrue(result.getData() instanceof AccountResponse);
        AccountResponse response = (AccountResponse) result.getData();
        assertEquals("Azhar", response.getFirstName());
        assertEquals("Haikal", response.getLastName());
        assertEquals("123456789", response.getPhone());
    }

    @Test
    void getAccountById_AccountNotFound() {
        // Arrange
        String token = "testtoken";
        Long accountId = 1L;

        when(accountRepository.findById(accountId)).thenReturn(Optional.empty());

        assertThrows(ErrorException.class, () -> accountService.getAccountById(token, accountId));

        verify(accountRepository, never()).save(any());
    }

    @Test
    void updateAccount_Success() {
        // Arrange
        String token = "testtoken";
        Long accountId = 1L;
        AccountRequest request = new AccountRequest();
        request.setFirstName("NewAzhar");
        request.setLastName("NewHaikal");
        request.setUsername("newazharhaikall");
        request.setPassword("newpassword");
        request.setPhone("987654321");
        request.setPin("4321");

        Customers existingCustomer = new Customers();
        existingCustomer.setCustomerId(1L);

        Account existingAccount = new Account();
        existingAccount.setAccountId(accountId);
        existingAccount.setCustomer(existingCustomer);
        when(accountRepository.findById(accountId)).thenReturn(Optional.of(existingAccount));
        when(passwordEncoder.encode(request.getPassword())).thenReturn("encodedPassword");

        // Act
        ApiResponseDataDTO result = accountService.updateAccount(token, accountId, request);

        // Assert
        assertTrue(result.getSuccess());
        assertNotNull(result.getData());
        assertTrue(result.getData() instanceof AccountResponse);
        AccountResponse response = (AccountResponse) result.getData();
        assertEquals("NewAzhar", response.getFirstName());
        assertEquals("NewHaikal", response.getLastName());
        assertEquals("newazharhaikall", response.getUsername());
        assertEquals("987654321", response.getPhone());
    }

    @Test
    void updateAccount_AccountNotFound() {
        // Arrange
        String token = "testtoken";
        Long accountId = 1L;
        AccountRequest request = new AccountRequest();

        when(accountRepository.findById(accountId)).thenReturn(Optional.empty());

        assertThrows(ErrorException.class, () -> accountService.updateAccount(token, accountId, request));

        verify(accountRepository, never()).save(any());
    }

    @Test
    void deleteAccountToken_Success() {
        // Arrange
        String token = "testtoken";
        Long customerId = 1L;

        Customers existingCustomer = new Customers();
        existingCustomer.setCustomerId(customerId);
        existingCustomer.setToken("existingToken");

        when(customerRepository.findById(customerId)).thenReturn(Optional.of(existingCustomer));

        // Act
        ApiResponseDataDTO result = accountService.deleteAccountToken(token, customerId);

        // Assert
        assertTrue(result.getSuccess());
        assertEquals("Success logout..", result.getData());
        assertNull(existingCustomer.getToken());
    }

    @Test
    void deleteAccountToken_CustomerNotFound() {
        // Arrange
        String token = "testtoken";
        Long customerId = 1L;

        when(customerRepository.findById(customerId)).thenReturn(Optional.empty());

        assertThrows(ErrorException.class, () -> accountService.deleteAccountToken(token, customerId));

        verify(accountRepository, never()).save(any());
    }
}
package miniprojectbank.indivarabank.controller;

import miniprojectbank.indivarabank.service.AccountStatementService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.io.IOException;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class AccountStatementControllerTest {

    @Mock
    private AccountStatementService accountStatementService;

    @InjectMocks
    private AccountStatementController accountStatementController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void getAccountStatementByAccountId_Success() throws Exception {
        MockMvc mockMvc = MockMvcBuilders.standaloneSetup(accountStatementController).build();

        String token = "yourAccessToken"; // Gantilah dengan token yang valid

        mockMvc.perform(MockMvcRequestBuilders.get("/api/account-statements/{accountId}", 123L)
                        .header("token", token)) // Menambahkan token ke header permintaan
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.header().string(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=account-statement.pdf"));

        verify(accountStatementService).generatePdfAccountStatementByAccountId(eq(token), any(Long.class), any(MockHttpServletResponse.class));
    }

    @Test
    void getAccountStatementByAccountId_ExceptionThrown_ReturnsInternalServerError() throws Exception {
        MockMvc mockMvc = MockMvcBuilders.standaloneSetup(accountStatementController).build();

        String token = "yourAccessToken"; // Gantilah dengan token yang valid

        doThrow(new IOException("Simulated IOException")).when(accountStatementService)
                .generatePdfAccountStatementByAccountId(eq(token), any(Long.class), any(MockHttpServletResponse.class));

        mockMvc.perform(MockMvcRequestBuilders.get("/api/account-statements/{accountId}", 123L)
                        .header("token", token)) // Menambahkan token ke header permintaan
                .andExpect(status().isInternalServerError());

        verify(accountStatementService).generatePdfAccountStatementByAccountId(eq(token), any(Long.class), any(MockHttpServletResponse.class));
    }
}
package miniprojectbank.indivarabank.controller;

import miniprojectbank.indivarabank.dto.transaction.DepositRequest;
import miniprojectbank.indivarabank.dto.transaction.DepositResponse;
import miniprojectbank.indivarabank.dto.custom.ApiResponseDataDTO;
import miniprojectbank.indivarabank.service.DepositService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

class DepositControllerTest {

    @Mock
    private DepositService depositService;

    @InjectMocks
    private DepositController depositController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testDeposit() {
        String token = "321231";
        Long accountId = 1L;
        DepositRequest request = new DepositRequest();
        ApiResponseDataDTO expectedResponse = new ApiResponseDataDTO(true, new DepositResponse());

        when(depositService.deposit(token, accountId, request)).thenReturn(expectedResponse);

        ResponseEntity<ApiResponseDataDTO> responseEntity = depositController.deposit(token, accountId, request);

        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        assertEquals(expectedResponse, responseEntity.getBody());
    }
}
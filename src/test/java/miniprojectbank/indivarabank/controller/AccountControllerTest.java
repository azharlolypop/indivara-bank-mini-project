package miniprojectbank.indivarabank.controller;

import miniprojectbank.indivarabank.dto.AccountRequest;
import miniprojectbank.indivarabank.dto.AccountResponse;
import miniprojectbank.indivarabank.dto.custom.ApiResponseDataDTO;
import miniprojectbank.indivarabank.service.AccountService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class AccountControllerTest {

    @Mock
    private AccountService accountService;

    @InjectMocks
    private AccountController accountController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void createAccount() {
        AccountRequest request = new AccountRequest();
        ApiResponseDataDTO expectedResponse = new ApiResponseDataDTO(true, new AccountResponse());

        when(accountService.createAccount(any())).thenReturn(expectedResponse);

        ResponseEntity<ApiResponseDataDTO> responseEntity = accountController.createAccount(request);

        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        assertEquals(expectedResponse, responseEntity.getBody());
    }

    @Test
    void createSecondAccount() {
        String token = "yourAccessToken";
        Long customerId = 1L;
        AccountRequest request = new AccountRequest();
        when(accountService.createSecondAccount(token, customerId, request)).thenReturn(new ApiResponseDataDTO(true, null));

        ResponseEntity<ApiResponseDataDTO> responseEntity = accountController.createSecondAccount(token, customerId, request);

        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        verify(accountService, times(1)).createSecondAccount(token, customerId, request);
    }

    @Test
    void updateAccount() {
        String token = "321231";
        Long accountId = 1L;
        AccountRequest request = new AccountRequest();
        ApiResponseDataDTO expectedResponse = new ApiResponseDataDTO(true, new AccountResponse());

        when(accountService.updateAccount(token, accountId, request)).thenReturn(expectedResponse);

        ResponseEntity<ApiResponseDataDTO> responseEntity = accountController.updateAccount(token, accountId, request);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(expectedResponse, responseEntity.getBody());
    }

    @Test
    void getAccountById() {
        String token = "321231";
        Long accountId = 1L;
        ApiResponseDataDTO expectedResponse =new ApiResponseDataDTO();

        when(accountService.getAccountById(token, accountId)).thenReturn(expectedResponse);

        ResponseEntity<ApiResponseDataDTO> responseEntity = accountController.getAccountById(token, accountId);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(expectedResponse, responseEntity.getBody());
    }

    @Test
    void deleteAccountToken() {
        String token = "321231";
        Long accountId = 1L;
        ApiResponseDataDTO expectedResponse = new ApiResponseDataDTO(true, "Token deleted successfully");

        when(accountService.deleteAccountToken(token, accountId)).thenReturn(expectedResponse);

        ResponseEntity<ApiResponseDataDTO> responseEntity = accountController.deleteAccountToken(token, accountId);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(expectedResponse, responseEntity.getBody());
        verify(accountService, times(1)).deleteAccountToken(token, accountId);
    }

}
package miniprojectbank.indivarabank.controller;

import miniprojectbank.indivarabank.dto.transaction.TransferRequest;
import miniprojectbank.indivarabank.dto.transaction.TransferResponse;
import miniprojectbank.indivarabank.dto.custom.ApiResponseDataDTO;
import miniprojectbank.indivarabank.service.TransferService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

class TransferControllerTest {

    @Mock
    private TransferService transferService;

    @InjectMocks
    private TransferController transferController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testTransfer() {
        String token = "321231";
        Long accountId = 1L;
        TransferRequest request = new TransferRequest();
        ApiResponseDataDTO expectedResponse = new ApiResponseDataDTO(true, new TransferResponse());

        when(transferService.transfer(token, accountId, request)).thenReturn(expectedResponse);

        ResponseEntity<ApiResponseDataDTO> responseEntity = transferController.transfer(token, accountId, request);

        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        assertEquals(expectedResponse, responseEntity.getBody());
    }
}
package miniprojectbank.indivarabank.controller;

import miniprojectbank.indivarabank.dto.transaction.WithdrawRequest;
import miniprojectbank.indivarabank.dto.transaction.WithdrawResponse;
import miniprojectbank.indivarabank.dto.custom.ApiResponseDataDTO;
import miniprojectbank.indivarabank.service.WithdrawService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

class WithdrawControllerTest {

    @Mock
    private WithdrawService withdrawService;

    @InjectMocks
    private WithdrawController withdrawController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testWithdraw() {
        String token = "321231";
        Long accountId = 1L;
        WithdrawRequest request = new WithdrawRequest();
        ApiResponseDataDTO expectedResponse = new ApiResponseDataDTO(true, new WithdrawResponse());

        when(withdrawService.withdraw(token, accountId, request)).thenReturn(expectedResponse);

        ResponseEntity<ApiResponseDataDTO> responseEntity = withdrawController.withdraw(token, accountId, request);

        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        assertEquals(expectedResponse, responseEntity.getBody());
    }
}
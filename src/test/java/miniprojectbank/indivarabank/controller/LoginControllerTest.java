package miniprojectbank.indivarabank.controller;

import miniprojectbank.indivarabank.dto.LoginRequest;
import miniprojectbank.indivarabank.dto.LoginResponse;
import miniprojectbank.indivarabank.dto.custom.ApiResponseDataDTO;
import miniprojectbank.indivarabank.entity.Customers;
import miniprojectbank.indivarabank.service.LoginService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class LoginControllerTest {

    @Mock
    private LoginService loginService;

    @InjectMocks
    private LoginController loginController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testLoginAccountSuccess() {
        LoginRequest request = new LoginRequest();
        request.setUsername("zhar.loly");
        request.setPassword("indivara123456");

        Customers customers = new Customers();
        customers.setCustomerId(1L);
        customers.setUsername("zhar.loly");
        customers.setPassword("hashed_password");
        customers.setToken("generated_token");

        LoginResponse loginResponse = new LoginResponse();
        loginResponse.setToken("generated_token");

        ApiResponseDataDTO apiResponseDataDTO = new ApiResponseDataDTO(true, loginResponse);

        when(loginService.loginCustomers(request)).thenReturn(apiResponseDataDTO);

        ResponseEntity<ApiResponseDataDTO> responseEntity = loginController.loginCustomers(request);

        assertNotNull(responseEntity);
        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        assertNotNull(responseEntity.getBody());
        assertTrue(responseEntity.getBody().getSuccess());
        assertNotNull(responseEntity.getBody().getData());

        LoginResponse response = (LoginResponse) responseEntity.getBody().getData();
        assertEquals("generated_token", response.getToken());

        verify(loginService, times(1)).loginCustomers(request);
    }

    @Test
    void testLoginAccountFailure() {
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setUsername("zhar.loly");
        loginRequest.setPassword("invalid_password");

        ApiResponseDataDTO apiResponseDataDTO = new ApiResponseDataDTO(false, null);

        when(loginService.loginCustomers(loginRequest)).thenReturn(apiResponseDataDTO);

        ResponseEntity<ApiResponseDataDTO> responseEntity = loginController.loginCustomers(loginRequest);

        assertNotNull(responseEntity);
        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        assertNotNull(responseEntity.getBody());
        assertFalse(responseEntity.getBody().getSuccess());
        assertNull(responseEntity.getBody().getData());

        verify(loginService, times(1)).loginCustomers(loginRequest);
    }
}
package miniprojectbank.indivarabank.repository;

import miniprojectbank.indivarabank.dto.mutation.TransactionDTO;
import miniprojectbank.indivarabank.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountStatementRepository extends JpaRepository<Account, Long> {

    @Query("SELECT new miniprojectbank.indivarabank.dto.mutation.TransactionDTO(" +
            "bd.depositId, 'deposit', bd.depositAmount, bd.depositDate) " +
            "FROM Account a LEFT JOIN Deposit bd ON a.accountId = bd.account.accountId " +
            "WHERE a.accountId = :accountId AND bd.depositId IS NOT NULL AND bd.depositAmount IS NOT NULL AND bd.depositDate IS NOT NULL")
    List<TransactionDTO> getDepositsByAccountId(@Param("accountId") Long accountId);

    @Query("SELECT new miniprojectbank.indivarabank.dto.mutation.TransactionDTO(" +
            "w.withdrawId, 'withdraw', w.withdrawAmount, w.withdrawDate) " +
            "FROM Account a LEFT JOIN Withdraw w ON a.accountId = w.account.accountId " +
            "WHERE a.accountId = :accountId AND w.withdrawId IS NOT NULL AND w.withdrawAmount IS NOT NULL AND w.withdrawDate IS NOT NULL")
    List<TransactionDTO> getWithdrawalsByAccountId(@Param("accountId") Long accountId);

    @Query("SELECT new miniprojectbank.indivarabank.dto.mutation.TransactionDTO(" +
            "t.transferId, 'transfer', t.transferAmount, t.transferDate) " +
            "FROM Account a LEFT JOIN Transfer t ON a.accountId = t.account.accountId " +
            "WHERE a.accountId = :accountId AND t.transferId IS NOT NULL AND t.transferAmount IS NOT NULL AND t.transferDate IS NOT NULL")
    List<TransactionDTO> getTransfersByAccountId(@Param("accountId") Long accountId);
}
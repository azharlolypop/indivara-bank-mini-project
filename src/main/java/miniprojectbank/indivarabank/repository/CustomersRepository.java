package miniprojectbank.indivarabank.repository;

import miniprojectbank.indivarabank.entity.Customers;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CustomersRepository extends JpaRepository<Customers, Long> {
    boolean existsByPhone(String phone);

    Optional<Customers> findByToken(String token);

    Optional<Customers> findByUsername(String username);

    boolean existsByUsername(String username);
}

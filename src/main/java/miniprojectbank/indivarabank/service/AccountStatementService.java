package miniprojectbank.indivarabank.service;

import com.lowagie.text.DocumentException;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import miniprojectbank.indivarabank.dto.mutation.AccountStatementDTO;
import miniprojectbank.indivarabank.dto.mutation.TransactionDTO;
import miniprojectbank.indivarabank.exception.ErrorException;
import miniprojectbank.indivarabank.repository.AccountRepository;
import miniprojectbank.indivarabank.repository.AccountStatementRepository;
import miniprojectbank.indivarabank.util.PdfGenerator;
import miniprojectbank.indivarabank.validation.TokenValidation;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class AccountStatementService {

    private final AccountRepository accountRepository;
    private final AccountStatementRepository accountStatementRepository;
    private final TokenValidation tokenValidation;

    public void generatePdfAccountStatementByAccountId(String token, Long accountId, HttpServletResponse response) throws IOException, DocumentException {
        log.info("Generating PDF account statement for Account ID: {}", accountId);
        try {
            tokenValidation.validateToken(token, accountId);

            String username = getUsernameByAccountId(accountId);

            List<TransactionDTO> deposits = new ArrayList<>(accountStatementRepository.getDepositsByAccountId(accountId));
            List<TransactionDTO> withdrawals = new ArrayList<>(accountStatementRepository.getWithdrawalsByAccountId(accountId));
            List<TransactionDTO> transfers = new ArrayList<>(accountStatementRepository.getTransfersByAccountId(accountId));

            List<TransactionDTO> transactions = concatTransactions(deposits, withdrawals, transfers);
            var statement = new AccountStatementDTO(accountId, username, transactions);

            PdfGenerator.generatePdf(username, response, transactions, statement);

            log.info("PDF account statement generated successfully for Account ID: {}", accountId);
        } catch (Exception e) {
            log.error("Error generating PDF account statement for Account ID: {}", accountId, e);
            throw e;
        }
    }

    private String getUsernameByAccountId(Long accountId) {
        return accountRepository.findById(accountId)
                .map(account -> {
                    log.info("Username found for Account ID: {}", accountId);
                    return account.getCustomer().getUsername();
                })
                .orElseThrow(() -> {
                    log.error("Failed to get username for Account ID: {}", accountId);
                    return new ErrorException(404, "Not Found", "Account not found with ID: " + accountId);
                });
    }

    private List<TransactionDTO> concatTransactions(List<TransactionDTO> deposits, List<TransactionDTO> withdrawals, List<TransactionDTO> transfers) {
        List<TransactionDTO> result = new ArrayList<>();
        result.addAll(deposits);
        result.addAll(withdrawals);
        result.addAll(transfers);
        return result;
    }
}
package miniprojectbank.indivarabank.service;

import lombok.RequiredArgsConstructor;
import miniprojectbank.indivarabank.dto.LoginRequest;
import miniprojectbank.indivarabank.dto.LoginResponse;
import miniprojectbank.indivarabank.dto.custom.ApiResponseDataDTO;
import miniprojectbank.indivarabank.entity.Customers;
import miniprojectbank.indivarabank.repository.CustomersRepository;
import miniprojectbank.indivarabank.util.JwtToken;
import miniprojectbank.indivarabank.validation.LoginValidation;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class LoginService {

    private final CustomersRepository customersRepository;
    private final LoginValidation loginValidation;

    public ApiResponseDataDTO loginCustomers(LoginRequest request) {
        var response = new LoginResponse();

        Customers validateAccount = loginValidation.validateLogin(request);
        String token = JwtToken.generateToken(request);
        validateAccount.setToken(token);
        customersRepository.save(validateAccount);
        response.setToken(token);

        return new ApiResponseDataDTO(true, response);
    }
}
package miniprojectbank.indivarabank.service;

import lombok.RequiredArgsConstructor;
import miniprojectbank.indivarabank.dto.transaction.TransferRequest;
import miniprojectbank.indivarabank.dto.transaction.TransferResponse;
import miniprojectbank.indivarabank.dto.custom.ApiResponseDataDTO;
import miniprojectbank.indivarabank.entity.Account;
import miniprojectbank.indivarabank.entity.Transfer;
import miniprojectbank.indivarabank.exception.ErrorException;
import miniprojectbank.indivarabank.repository.AccountRepository;
import miniprojectbank.indivarabank.repository.TransferRepository;
import miniprojectbank.indivarabank.validation.TokenValidation;
import miniprojectbank.indivarabank.validation.TransferValidation;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class TransferService {

    private final TransferRepository transferRepository;
    private final AccountRepository accountRepository;
    private final TransferValidation transferValidation;
    private final TokenValidation tokenValidation;

    @Transactional
    public ApiResponseDataDTO transfer(String token, Long accountId, TransferRequest request) {
        tokenValidation.validateToken(token, accountId);

        var account = accountRepository.findById(accountId)
                .orElseThrow(() -> new ErrorException(404, "Not Found", "Source account not found with ID: " + accountId));

        transferValidation.validateTransferRequest(account, request);

        var destinationAccount = (Account) accountRepository
                .findByAccountNumber(request.getAccountNumberDestination())
                .orElseThrow(() -> new ErrorException(404, "Not Found", "Destination account not found"));

        var transfer = new Transfer();
        transfer.setTransferDate(LocalDateTime.now());
        transfer.setTransferAmount(request.getTransferAmount());
        transfer.setAccountNumberDestination(request.getAccountNumberDestination());
        transfer.setAccount(account);

        account.setPin(request.getPin());
        account.setTotalBalance(account.getTotalBalance().subtract(request.getTransferAmount()));
        accountRepository.save(account);

        destinationAccount.setTotalBalance(destinationAccount.getTotalBalance().add(request.getTransferAmount()));
        accountRepository.save(destinationAccount);
        transfer = transferRepository.save(transfer);

        var response = new TransferResponse();
        BeanUtils.copyProperties(transfer, response);
        response.setAccountId(accountId);
        response.setUsername(destinationAccount.getCustomer().getUsername());
        return new ApiResponseDataDTO(true, response);
    }
}
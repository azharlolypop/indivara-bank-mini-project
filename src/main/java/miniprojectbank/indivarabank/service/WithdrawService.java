package miniprojectbank.indivarabank.service;

import lombok.RequiredArgsConstructor;
import miniprojectbank.indivarabank.dto.custom.ApiResponseDataDTO;
import miniprojectbank.indivarabank.dto.transaction.WithdrawRequest;
import miniprojectbank.indivarabank.dto.transaction.WithdrawResponse;
import miniprojectbank.indivarabank.entity.Withdraw;
import miniprojectbank.indivarabank.exception.ErrorException;
import miniprojectbank.indivarabank.repository.AccountRepository;
import miniprojectbank.indivarabank.repository.WithdrawRepository;
import miniprojectbank.indivarabank.validation.TokenValidation;
import miniprojectbank.indivarabank.validation.WithdrawValidation;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class WithdrawService {

    private final WithdrawRepository withdrawRepository;
    private final AccountRepository accountRepository;
    private final WithdrawValidation withdrawValidation;
    private final TokenValidation tokenValidation;

    @Transactional
    public ApiResponseDataDTO withdraw(String token, Long accountId, WithdrawRequest request) {
        tokenValidation.validateToken(token, accountId);

        var account = accountRepository.findById(accountId)
                .orElseThrow(() -> new ErrorException(404, "Not Found", "Customer not found with ID: " + accountId));

        withdrawValidation.validateWithdrawRequest(account, request);

        var withdraw = new Withdraw();
        withdraw.setWithdrawDate(LocalDateTime.now());
        withdraw.setWithdrawAmount(request.getWithdrawAmount());
        withdraw.setAccount(account);

        account.setPin(request.getPin());
        account.setTotalBalance(account.getTotalBalance().subtract(request.getWithdrawAmount()));
        accountRepository.save(account);
        withdraw = withdrawRepository.save(withdraw);

        var response = new WithdrawResponse();
        BeanUtils.copyProperties(withdraw, response);
        response.setAccountId(accountId);
        return new ApiResponseDataDTO(true, response);
    }
}
package miniprojectbank.indivarabank.service;

import lombok.RequiredArgsConstructor;
import miniprojectbank.indivarabank.dto.custom.ApiResponseDataDTO;
import miniprojectbank.indivarabank.dto.transaction.DepositRequest;
import miniprojectbank.indivarabank.dto.transaction.DepositResponse;
import miniprojectbank.indivarabank.entity.Deposit;
import miniprojectbank.indivarabank.exception.ErrorException;
import miniprojectbank.indivarabank.repository.AccountRepository;
import miniprojectbank.indivarabank.repository.DepositRepository;
import miniprojectbank.indivarabank.validation.DepositValidation;
import miniprojectbank.indivarabank.validation.TokenValidation;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class DepositService {

    private final DepositRepository depositRepository;
    private final AccountRepository accountRepository;
    private final DepositValidation depositValidation;
    private final TokenValidation tokenValidation;

    @Transactional
    public ApiResponseDataDTO deposit(String token, Long accountId, DepositRequest request) {
        tokenValidation.validateToken(token, accountId);

        var account = accountRepository.findById(accountId)
                .orElseThrow(() -> new ErrorException(404, "Not Found", "Customer not found with ID: " + accountId));

        depositValidation.validateDepositRequest(account, request);

        var balanceDeposit = new Deposit();
        balanceDeposit.setDepositDate(LocalDateTime.now());
        balanceDeposit.setDepositAmount(request.getDepositAmount());
        balanceDeposit.setAccount(account);

        account.setPin(request.getPin());
        account.setTotalBalance(account.getTotalBalance().add(request.getDepositAmount()));
        accountRepository.save(account);
        balanceDeposit = depositRepository.save(balanceDeposit);

        var response = new DepositResponse();
        BeanUtils.copyProperties(balanceDeposit, response);
        response.setAccountId(accountId);
        return new ApiResponseDataDTO(true, response);
    }
}
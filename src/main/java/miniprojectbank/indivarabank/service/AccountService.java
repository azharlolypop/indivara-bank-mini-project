package miniprojectbank.indivarabank.service;

import lombok.RequiredArgsConstructor;
import miniprojectbank.indivarabank.dto.AccountRequest;
import miniprojectbank.indivarabank.dto.AccountResponse;
import miniprojectbank.indivarabank.dto.custom.ApiResponseDataDTO;
import miniprojectbank.indivarabank.entity.Account;
import miniprojectbank.indivarabank.entity.Customers;
import miniprojectbank.indivarabank.exception.ErrorException;
import miniprojectbank.indivarabank.repository.AccountRepository;
import miniprojectbank.indivarabank.repository.CustomersRepository;
import miniprojectbank.indivarabank.validation.AccountValidation;
import miniprojectbank.indivarabank.validation.TokenValidation;
import org.springframework.beans.BeanUtils;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Random;

@Service
@RequiredArgsConstructor
public class AccountService {

    private final AccountRepository accountRepository;
    private final CustomersRepository customerRepository;
    private final BCryptPasswordEncoder passwordEncoder;
    private final AccountValidation accountValidation;
    private final TokenValidation tokenValidation;
    private final Random random = new Random();
    private static final String NOT_FOUND = "Not Found";
    private static final String CUSTOMER_NOT_FOUND = "Account not found with ID: ";

    @Transactional
    public ApiResponseDataDTO createAccount(AccountRequest request) {
        accountValidation.validateCreateAccountRequest(request);

        var customers = new Customers();
        BeanUtils.copyProperties(request, customers);
        customers.setPassword(passwordEncoder.encode(request.getPassword()));
        customerRepository.save(customers);

        var account = new Account();
        BeanUtils.copyProperties(request, account);
        account.setAccountNumber(generateAccountNumber());
        account.setTotalBalance(request.getDepositAmount());
        account.setCustomer(customers);
        accountRepository.save(account);

        var response = new AccountResponse();
        BeanUtils.copyProperties(account, response);
        BeanUtils.copyProperties(customers, response);
        return new ApiResponseDataDTO(true, response);
    }

    @Transactional
    public ApiResponseDataDTO createSecondAccount(String token, Long customerId, AccountRequest request) {
        tokenValidation.validateToken(token, customerId);

        accountValidation.validateCreateSecondAccountRequest(request);

        var customers = customerRepository.findById(customerId)
                .orElseThrow(() -> new ErrorException(404, NOT_FOUND, CUSTOMER_NOT_FOUND + customerId));

        var account = new Account();
        account.setPin(request.getPin());
        account.setAccountNumber(generateAccountNumber());
        account.setTotalBalance(request.getDepositAmount());
        account.setCustomer(customers);
        accountRepository.save(account);

        var response = new AccountResponse();
        BeanUtils.copyProperties(account, response);
        BeanUtils.copyProperties(customers, response);
        return new ApiResponseDataDTO(true, response);
    }

    public ApiResponseDataDTO getAccountById(String token, Long accountId) {
        tokenValidation.validateToken(token, accountId);

        var account = accountRepository.findById(accountId)
                .orElseThrow(() -> new ErrorException(404, NOT_FOUND, CUSTOMER_NOT_FOUND + accountId));

        var response = new AccountResponse();
        BeanUtils.copyProperties(account, response);
        response.setFirstName(account.getCustomer().getFirstName());
        response.setLastName(account.getCustomer().getLastName());
        response.setPhone(account.getCustomer().getPhone());
        response.setUsername(account.getCustomer().getUsername());
        return new ApiResponseDataDTO(true, response);
    }

    @Transactional
    public ApiResponseDataDTO updateAccount(String token, Long accountId, AccountRequest request) {
        tokenValidation.validateToken(token, accountId);

        accountValidation.validateUpdateAccountRequest(request);

        var account = accountRepository.findById(accountId)
                .orElseThrow(() -> new ErrorException(404, NOT_FOUND, CUSTOMER_NOT_FOUND + accountId));

        var customers = account.getCustomer();
        customers.setFirstName(request.getFirstName());
        customers.setLastName(request.getLastName());
        customers.setUsername(request.getUsername());
        customers.setPassword(passwordEncoder.encode(request.getPassword()));
        customers.setPhone(request.getPhone());

        account.setCustomer(customers);
        account.setPin(request.getPin());
        accountRepository.save(account);

        var response = new AccountResponse();
        BeanUtils.copyProperties(account, response);
        BeanUtils.copyProperties(customers, response);
        return new ApiResponseDataDTO(true, response);
    }

    @Transactional
    public ApiResponseDataDTO deleteAccountToken(String token, Long idCustomer) {
        tokenValidation.validateToken(token, idCustomer);

        var customers = customerRepository.findById(idCustomer)
                .orElseThrow(() -> new ErrorException(404, NOT_FOUND, CUSTOMER_NOT_FOUND + idCustomer));

        customers.setToken(null);
        customerRepository.save(customers);

        return new ApiResponseDataDTO(true, "Success logout..");
    }

    private String generateAccountNumber() {
        var accountNumber = new StringBuilder();
        for (var i = 0; i < 9; i++) {
            var digit = random.nextInt(10);
            accountNumber.append(digit);
        }
        return accountNumber.toString();
    }
}
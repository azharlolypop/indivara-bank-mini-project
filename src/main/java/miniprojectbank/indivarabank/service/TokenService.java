package miniprojectbank.indivarabank.service;

import lombok.RequiredArgsConstructor;
import miniprojectbank.indivarabank.entity.Customers;
import miniprojectbank.indivarabank.repository.CustomersRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class TokenService {

    private final CustomersRepository customersRepository;

    public boolean getToken(String token) {
        Optional<Customers> getToken = customersRepository.findByToken(token);
        return getToken.isPresent();
    }
}
package miniprojectbank.indivarabank.exception;

import miniprojectbank.indivarabank.dto.custom.ErrorResponse;

public class ErrorException extends RuntimeException {

    private final int status;
    private final String error;

    public ErrorException(int status, String error, String message) {
        super(message);
        this.status = status;
        this.error = error;
    }

    public ErrorResponse toErrorResponse() {
        return new ErrorResponse(status, error, getMessage());
    }
}
package miniprojectbank.indivarabank.util;

import com.lowagie.text.Font;
import com.lowagie.text.*;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import jakarta.servlet.http.HttpServletResponse;
import miniprojectbank.indivarabank.dto.mutation.AccountStatementDTO;
import miniprojectbank.indivarabank.dto.mutation.TransactionDTO;

import java.awt.*;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;

public class PdfGenerator {

    public static void generatePdf(String username, HttpServletResponse response, List<TransactionDTO> transactions, AccountStatementDTO statement) throws IOException, DocumentException {
        try (var document = new Document(PageSize.A4)) {
            PdfWriter.getInstance(document, response.getOutputStream());
            document.open();

            addTitle(document);
            addCustomerInfo(document, statement, username);

            var table = new PdfPTable(4);
            table.setWidthPercentage(100);

            addTableHeader(table);
            addRows(table, transactions);

            document.add(table);
        } catch (DocumentException e) {
            throw new IOException("Error generating PDF..", e);
        }
    }

    private static void addTitle(Document document) throws DocumentException {
        var titleFont = FontFactory.getFont(FontFactory.HELVETICA, 16, Font.BOLD, new Color(0, 0, 0));
        var titleParagraph = new Paragraph("Transaction History", titleFont);
        titleParagraph.setAlignment(Element.ALIGN_CENTER);
        document.add(titleParagraph);
        document.add(new Paragraph("\n"));
    }

    private static void addCustomerInfo(Document document, AccountStatementDTO statement, String username) throws DocumentException {
        var customerFont = FontFactory.getFont(FontFactory.HELVETICA, 10, Font.NORMAL, new Color(0, 0, 0));

        var customerParagraph = new Paragraph(
                "Account ID: " + statement.getAccountId() + "\n" +
                        "Full Name: " + username + "\n" +
                        "Transaction History Date: " + LocalDate.now(), customerFont);

        document.add(customerParagraph);
        document.add(new Paragraph("\n"));
    }

    private static void addTableHeader(PdfPTable table) {
        var header = new String[]{"ID", "Type", "Transaction Date", "Amount"};

        for (var columnHeader : header) {
            var headerCell = new PdfPCell();
            headerCell.setBackgroundColor(new Color(86, 160, 239));
            headerCell.setBorderWidth(1);

            headerCell.setHorizontalAlignment(Element.ALIGN_CENTER);
            headerCell.setVerticalAlignment(Element.ALIGN_MIDDLE);

            headerCell.setPhrase(new Phrase(columnHeader));
            table.addCell(headerCell);
        }
    }

    private static void addRows(PdfPTable table, List<TransactionDTO> transactions) {
        var currencyFormat = NumberFormat.getCurrencyInstance(new Locale("id", "ID"));
        var dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        BigDecimal totalAmount = BigDecimal.ZERO;

        for (TransactionDTO transaction : transactions) {
            table.addCell(String.valueOf(transaction.getTransactionId()));
            table.addCell(transaction.getType());
            String formattedDate = transaction.getTransactionDate().format(dateFormatter);
            table.addCell(formattedDate);

            // Format amount in "Rp 10.000.000,00,-" format
            String formattedAmount = currencyFormat.format(transaction.getAmount());
            formattedAmount = formattedAmount.replace("Rp", "Rp. ");
            formattedAmount += ",-";
            table.addCell(formattedAmount);

            // Update total amount
            totalAmount = totalAmount.add(transaction.getAmount());
        }

        // Add total row
        addTotalRow(table, totalAmount, currencyFormat);
    }

    private static void addTotalRow(PdfPTable table, BigDecimal totalAmount, NumberFormat currencyFormat) {
        var totalCell = new PdfPCell(new Phrase("Total", FontFactory.getFont(FontFactory.HELVETICA, 12, Font.BOLD)));
        totalCell.setBackgroundColor(new Color(86, 160, 239));
        totalCell.setBorderWidth(1);
        totalCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        totalCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        table.addCell(totalCell);

        // Add empty cells for Type and Date columns
        table.addCell("");
        table.addCell("");

        // Format total amount in "Rp 10.000.000,00,-" format
        String formattedTotal = currencyFormat.format(totalAmount);
        formattedTotal = formattedTotal.replace("Rp", "Rp ");
        formattedTotal += ",-";
        var totalAmountCell = new PdfPCell(new Phrase(formattedTotal, FontFactory.getFont(FontFactory.HELVETICA, 12, Font.BOLD)));
        totalAmountCell.setBackgroundColor(new Color(86, 160, 239));
        totalAmountCell.setBorderWidth(1);
        totalAmountCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        totalAmountCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        table.addCell(totalAmountCell);
    }

    private PdfGenerator() {
    }
}
package miniprojectbank.indivarabank.util;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.crypto.MacProvider;

import java.security.Key;
import java.util.Date;

public class JwtToken {

    private JwtToken() {
        throw new IllegalStateException("Utility Class");
    }

    public static String generateToken(Object request) {
        try {
            long currentMillis = System.currentTimeMillis();
            var date = new Date(currentMillis);
            Key key = MacProvider.generateKey();

            return Jwts.builder()
                    .setSubject(String.valueOf(request))
                    .setIssuedAt(date)
                    .signWith(SignatureAlgorithm.HS512, key)
                    .compact();
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }
}

package miniprojectbank.indivarabank.dto;

import lombok.Data;

@Data
public class LoginResponse {

    private String token;
}

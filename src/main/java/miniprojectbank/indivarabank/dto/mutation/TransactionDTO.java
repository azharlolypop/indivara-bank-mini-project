package miniprojectbank.indivarabank.dto.mutation;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
public class TransactionDTO {

    private Long transactionId;
    private String type;
    private BigDecimal amount;
    private LocalDateTime transactionDate;
}

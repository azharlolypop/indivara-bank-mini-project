package miniprojectbank.indivarabank.dto.mutation;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class AccountStatementDTO {

    private Long accountId;
    private String username;
    private List<TransactionDTO> historyTransaction;
}

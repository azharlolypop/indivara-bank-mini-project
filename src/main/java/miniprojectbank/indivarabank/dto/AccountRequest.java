package miniprojectbank.indivarabank.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class AccountRequest {

    private String firstName;
    private String lastName;
    private String phone;
    private String username;
    private String password;
    private String pin;
    private BigDecimal depositAmount;
}
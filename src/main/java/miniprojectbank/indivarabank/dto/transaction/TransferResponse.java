package miniprojectbank.indivarabank.dto.transaction;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class TransferResponse {

    private Long transferId;
    private Long accountId;
    private String username;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime transferDate;

    private BigDecimal transferAmount;
    private String accountNumberDestination;
}

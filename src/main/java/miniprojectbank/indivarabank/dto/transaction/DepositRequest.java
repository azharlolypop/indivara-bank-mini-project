package miniprojectbank.indivarabank.dto.transaction;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class DepositRequest {

    private BigDecimal depositAmount;
    private String pin;
}

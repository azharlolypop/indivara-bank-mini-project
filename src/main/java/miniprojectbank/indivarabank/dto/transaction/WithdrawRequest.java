package miniprojectbank.indivarabank.dto.transaction;

import lombok.Data;
import java.math.BigDecimal;

@Data
public class WithdrawRequest {

    private BigDecimal withdrawAmount;
    private String pin;
}
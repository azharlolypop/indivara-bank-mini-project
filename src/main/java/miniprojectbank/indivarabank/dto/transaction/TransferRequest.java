package miniprojectbank.indivarabank.dto.transaction;

import lombok.Data;
import java.math.BigDecimal;

@Data
public class TransferRequest {

    private BigDecimal transferAmount;
    private String pin;
    private String accountNumberDestination;
}

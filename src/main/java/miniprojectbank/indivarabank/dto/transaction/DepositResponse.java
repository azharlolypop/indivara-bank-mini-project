package miniprojectbank.indivarabank.dto.transaction;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class DepositResponse {

    private Long depositId;
    private Long accountId;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime depositDate;

    private BigDecimal depositAmount;
}

package miniprojectbank.indivarabank.dto.custom;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ApiResponseDataDTO {
    private Boolean success;
    private Object data;

    public ApiResponseDataDTO apiResponseDataDTO (Boolean success ,Object data) {
        this.success = success;
        this.data = data;
        return this;
    }
}

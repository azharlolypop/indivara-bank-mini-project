package miniprojectbank.indivarabank.dto;

import lombok.Data;
import java.math.BigDecimal;

@Data
public class AccountResponse {

    private Long accountId;
    private String firstName;
    private String lastName;
    private String phone;
    private String username;
    private String accountNumber;
    private BigDecimal totalBalance;
}

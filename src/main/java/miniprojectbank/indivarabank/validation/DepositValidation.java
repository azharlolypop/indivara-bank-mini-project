package miniprojectbank.indivarabank.validation;

import miniprojectbank.indivarabank.dto.transaction.DepositRequest;
import miniprojectbank.indivarabank.entity.Account;
import miniprojectbank.indivarabank.exception.ErrorException;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Objects;

@Service
public class DepositValidation {

    public void validateDepositRequest(Account account, DepositRequest request) {
        validatePin(account, request.getPin());
        validateDeposit(request.getDepositAmount());
    }

    private void validatePin(Account account, String pin) {
        if (!account.getPin().equals(pin)) {
            throw new ErrorException(400, "Bad Request", "Incorrect PIN");
        }
    }

    private void validateDeposit(BigDecimal depositAmount) {
        if (Objects.isNull(depositAmount) || depositAmount.compareTo(BigDecimal.ZERO) <= 0) {
            throw new ErrorException(400, "Bad Request", "'depositAmount' must be a positive non-zero value.");
        }
    }
}
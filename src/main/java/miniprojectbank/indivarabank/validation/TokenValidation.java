package miniprojectbank.indivarabank.validation;

import lombok.RequiredArgsConstructor;
import miniprojectbank.indivarabank.exception.ErrorException;
import miniprojectbank.indivarabank.repository.AccountRepository;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class TokenValidation {

    private final AccountRepository accountRepository;

    public void validateToken(String token, Long customerId) {
        var account = accountRepository.findById(customerId)
                .orElseThrow(() -> new ErrorException(404, "Not Found", "Customer not found with ID: " + customerId));

        if (!account.getCustomer().getToken().equals(token)) {
            throw new ErrorException(401, "Unauthorized", "Invalid token for customer ID: " + customerId);
        }
    }
}

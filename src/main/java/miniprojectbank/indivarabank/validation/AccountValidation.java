package miniprojectbank.indivarabank.validation;

import lombok.RequiredArgsConstructor;
import miniprojectbank.indivarabank.dto.AccountRequest;
import miniprojectbank.indivarabank.exception.ErrorException;
import miniprojectbank.indivarabank.repository.CustomersRepository;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
@RequiredArgsConstructor
public class AccountValidation {

    private final CustomersRepository customersRepository;
    private static final int MAX_LENGTH = 20;
    private static final String BAD_REQUEST = "Bad Request";
    private static final String VALIDATE_PIN = "PIN must be exactly 6 numeric digits";

    public void validateCreateAccountRequest(AccountRequest request) {
        validateName(request.getFirstName(), "First Name");
        validateName(request.getLastName(), "Last Name");
        validatePhone(request.getPhone());
        validateCreateAccount("Username", request.getUsername(), MAX_LENGTH);
        validateCreateAccount("Password", request.getPassword(), Integer.MAX_VALUE);
        validatePin(request.getPin());
        validateDepositAmount(request.getDepositAmount());
        validateUsername(request.getUsername());
        validatePhoneExistence(request.getPhone());
    }

    public void validateCreateSecondAccountRequest(AccountRequest request) {
        validatePin(request.getPin());
        validateDepositAmount(request.getDepositAmount());
    }

    public void validateUpdateAccountRequest(AccountRequest request) {
        validateName(request.getFirstName(), "First Name");
        validateName(request.getLastName(), "Last Name");
        validatePhone(request.getPhone());
        validateCreateAccount("Username", request.getUsername(), MAX_LENGTH);
        validateCreateAccount("Password", request.getPassword(), Integer.MAX_VALUE);
        validatePin(request.getPin());
        validateUsername(request.getUsername());
        validatePhoneExistence(request.getPhone());
    }

    private void validateCreateAccount(String fieldName, String value, int maxLength) {
        validateNotBlank(value, fieldName);
        validateMaxLength(value, fieldName, maxLength);
    }

    private void validatePin(String pin) {
        if (pin == null || !pin.matches("\\d{6}")) {
            throw new ErrorException(400, BAD_REQUEST, VALIDATE_PIN);
        }
    }

    private void validateDepositAmount(BigDecimal depositAmount) {
        if (depositAmount == null || depositAmount.compareTo(new BigDecimal("50000")) < 0) {
            throw new ErrorException(400, BAD_REQUEST, "Deposit amount must be provided and must be at least 50000");
        }
    }

    private void validateUsername(String username) {
        if (customersRepository.existsByUsername(username)) {
            throw new ErrorException(400, BAD_REQUEST, "Username is already taken");
        }
    }

    private void validatePhoneExistence(String phone) {
        if (customersRepository.existsByPhone(phone)) {
            throw new ErrorException(400, BAD_REQUEST, "Phone number is already registered");
        }
    }

    private void validateNotBlank(String value, String fieldName) {
        if (StringUtils.isBlank(value)) {
            throw new ErrorException(400, BAD_REQUEST, fieldName + " cannot be null or blank");
        }
    }

    private void validateMaxLength(String value, String fieldName, int maxLength) {
        if (value.length() > maxLength) {
            throw new ErrorException(400, BAD_REQUEST, fieldName + " exceeds maximum length of " + maxLength + " characters");
        }
    }

    private void validatePhone(String phone) {
        validateNotBlank(phone, "Phone number");
        if (!StringUtils.isNumeric(phone)) {
            throw new ErrorException(400, BAD_REQUEST, "Phone number must contain only numeric digits");
        }
        if (phone.length() > 15) {
            throw new ErrorException(400, BAD_REQUEST, "Phone number must be no more than 15 characters");
        }
    }

    private void validateName(String name, String fieldName) {
        validateNotBlank(name, fieldName);
        if (!name.matches("^[a-zA-Z]+$")) {
            throw new ErrorException(400, BAD_REQUEST, fieldName + " should only contain alphabetic characters");
        }
        validateMaxLength(name, fieldName, MAX_LENGTH);
    }
}
package miniprojectbank.indivarabank.validation;

import lombok.RequiredArgsConstructor;
import miniprojectbank.indivarabank.dto.LoginRequest;
import miniprojectbank.indivarabank.entity.Customers;
import miniprojectbank.indivarabank.exception.ErrorException;
import miniprojectbank.indivarabank.repository.CustomersRepository;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class LoginValidation {

    private final CustomersRepository customersRepository;
    private final BCryptPasswordEncoder passwordEncoder;

    public Customers validateLogin(LoginRequest request) {
        Optional<Customers> optionalAccount = customersRepository.findByUsername(request.getUsername());

        if (optionalAccount.isPresent()) {
            var customers = optionalAccount.get();

            if (passwordEncoder.matches(request.getPassword(), customers.getPassword())) {
                return customers;
            } else {
                throw new ErrorException(400, "Bad Request", "Incorrect username and password");
            }
        } else {
            throw new ErrorException(400, "Bad Request", "Incorrect username and password");
        }
    }
}
package miniprojectbank.indivarabank.validation;

import miniprojectbank.indivarabank.dto.transaction.TransferRequest;
import miniprojectbank.indivarabank.entity.Account;
import miniprojectbank.indivarabank.exception.ErrorException;
import org.springframework.stereotype.Service;
import java.math.BigDecimal;
import java.util.Objects;

@Service
public class TransferValidation {

    private static final String BAD_REQUEST = "Bad Request";

    public void validateTransferRequest(Account account, TransferRequest request) {
        validatePin(account, request.getPin());
        validateTransferAmount(request.getTransferAmount());
        validateSufficientBalance(account, request.getTransferAmount());
        validateNotSelfTransfer(account.getAccountNumber(), request.getAccountNumberDestination());
    }

    private void validatePin(Account account, String pin) {
        if (!account.getPin().equals(pin)) {
            throw new ErrorException(400, BAD_REQUEST, "Incorrect PIN");
        }
    }

    private void validateTransferAmount(BigDecimal transferAmount) {
        if (Objects.isNull(transferAmount) || transferAmount.compareTo(BigDecimal.ZERO) <= 0) {
            throw new ErrorException(400, BAD_REQUEST, "'transferAmount' must be a positive non-zero value.");
        }
    }

    private void validateSufficientBalance(Account account, BigDecimal transferAmount) {
        if (account.getTotalBalance().compareTo(transferAmount) < 0) {
            throw new ErrorException(400, BAD_REQUEST, "Insufficient balance");
        }
    }

    private void validateNotSelfTransfer(String sourceAccountNumber, String destinationAccountNumber) {
        if (sourceAccountNumber.equals(destinationAccountNumber)) {
            throw new ErrorException(400, BAD_REQUEST, "Cannot transfer to your own account");
        }
    }
}
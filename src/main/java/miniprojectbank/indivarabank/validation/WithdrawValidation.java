package miniprojectbank.indivarabank.validation;

import miniprojectbank.indivarabank.dto.transaction.WithdrawRequest;
import miniprojectbank.indivarabank.entity.Account;
import miniprojectbank.indivarabank.exception.ErrorException;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Objects;

@Service
public class WithdrawValidation {

    private static final String BAD_REQUEST = "Bad Request";

    public void validateWithdrawRequest(Account account, WithdrawRequest request) {
        validatePin(account, request.getPin());
        validateWithdrawAmount(request.getWithdrawAmount());
        validateSufficientBalance(account, request.getWithdrawAmount());
    }

    private void validatePin(Account account, String pin) {
        if (!account.getPin().equals(pin)) {
            throw new ErrorException(400, BAD_REQUEST, "Incorrect PIN");
        }
    }

    private void validateWithdrawAmount(BigDecimal withdrawAmount) {
        if (Objects.isNull(withdrawAmount) || withdrawAmount.compareTo(BigDecimal.ZERO) <= 0) {
            throw new ErrorException(400, BAD_REQUEST, "'withdrawAmount' must be a positive non-zero value.");
        }
    }

    private void validateSufficientBalance(Account account, BigDecimal withdrawAmount) {
        if (account.getTotalBalance().compareTo(withdrawAmount) < 0) {
            throw new ErrorException(400, BAD_REQUEST, "Insufficient balance");
        }
    }
}

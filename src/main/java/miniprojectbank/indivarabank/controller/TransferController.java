package miniprojectbank.indivarabank.controller;

import lombok.RequiredArgsConstructor;
import miniprojectbank.indivarabank.dto.transaction.TransferRequest;
import miniprojectbank.indivarabank.dto.custom.ApiResponseDataDTO;
import miniprojectbank.indivarabank.service.TransferService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/transfer")
public class TransferController {

    private final TransferService transferService;

    @PostMapping("/{accountId}")
    public ResponseEntity<ApiResponseDataDTO> transfer(
            @RequestHeader ("token") String token,
            @PathVariable Long accountId,
            @RequestBody TransferRequest request) {
        return new ResponseEntity<>(transferService.transfer(token, accountId, request), HttpStatus.CREATED);
    }
}
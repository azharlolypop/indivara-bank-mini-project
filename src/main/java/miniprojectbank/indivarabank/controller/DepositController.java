package miniprojectbank.indivarabank.controller;

import lombok.RequiredArgsConstructor;
import miniprojectbank.indivarabank.dto.transaction.DepositRequest;
import miniprojectbank.indivarabank.dto.custom.ApiResponseDataDTO;
import miniprojectbank.indivarabank.service.DepositService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/deposit")
public class DepositController {

    private final DepositService depositService;

    @PostMapping("/{accountId}")
    public ResponseEntity<ApiResponseDataDTO> deposit(
            @RequestHeader ("token") String token,
            @PathVariable Long accountId,
            @RequestBody DepositRequest request) {
        return new ResponseEntity<>(depositService.deposit(token, accountId, request), HttpStatus.CREATED);
    }
}
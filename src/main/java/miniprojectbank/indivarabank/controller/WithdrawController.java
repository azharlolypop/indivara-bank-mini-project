package miniprojectbank.indivarabank.controller;

import lombok.RequiredArgsConstructor;
import miniprojectbank.indivarabank.dto.transaction.WithdrawRequest;
import miniprojectbank.indivarabank.dto.custom.ApiResponseDataDTO;
import miniprojectbank.indivarabank.service.WithdrawService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/withdraw")
public class WithdrawController {

    private final WithdrawService withdrawService;

    @PostMapping("/{accountId}")
    public ResponseEntity<ApiResponseDataDTO> withdraw(
            @RequestHeader ("token") String token,
            @PathVariable Long accountId,
            @RequestBody WithdrawRequest request) {
        return new ResponseEntity<>(withdrawService.withdraw(token, accountId, request), HttpStatus.CREATED);
    }
}
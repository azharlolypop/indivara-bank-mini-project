package miniprojectbank.indivarabank.controller;

import lombok.RequiredArgsConstructor;
import miniprojectbank.indivarabank.dto.AccountRequest;
import miniprojectbank.indivarabank.dto.custom.ApiResponseDataDTO;
import miniprojectbank.indivarabank.service.AccountService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/accounts")
@RequiredArgsConstructor
public class AccountController {

    private final AccountService accountService;

    @PostMapping
    public ResponseEntity<ApiResponseDataDTO> createAccount(
            @RequestBody AccountRequest request) {
        return new ResponseEntity<>(accountService.createAccount(request), HttpStatus.CREATED);
    }

    @PostMapping("/{customerId}")
    public ResponseEntity<ApiResponseDataDTO> createSecondAccount(
            @RequestHeader("token") String token,
            @PathVariable Long customerId,
            @RequestBody AccountRequest request) {
        return new ResponseEntity<>(accountService.createSecondAccount(token, customerId, request), HttpStatus.CREATED);
    }

    @GetMapping("/{accountId}")
    public ResponseEntity<ApiResponseDataDTO> getAccountById(
            @RequestHeader ("token") String token,
            @PathVariable Long accountId) {
        return new ResponseEntity<>(accountService.getAccountById(token, accountId), HttpStatus.OK);
    }

    @PutMapping("/{accountId}")
    public ResponseEntity<ApiResponseDataDTO> updateAccount(
            @RequestHeader ("token") String token,
            @PathVariable Long accountId,
            @RequestBody AccountRequest request) {
        return new ResponseEntity<>(accountService.updateAccount(token, accountId, request), HttpStatus.OK);
    }

    @DeleteMapping("/{customerId}")
    public ResponseEntity<ApiResponseDataDTO> deleteAccountToken(
            @RequestHeader("token") String token,
            @PathVariable Long customerId) {
        ApiResponseDataDTO response = accountService.deleteAccountToken(token, customerId);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
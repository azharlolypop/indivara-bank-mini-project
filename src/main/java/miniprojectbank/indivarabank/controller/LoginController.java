package miniprojectbank.indivarabank.controller;

import lombok.RequiredArgsConstructor;
import miniprojectbank.indivarabank.dto.LoginRequest;
import miniprojectbank.indivarabank.dto.custom.ApiResponseDataDTO;
import miniprojectbank.indivarabank.service.LoginService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/auth")
public class LoginController {

    private final LoginService loginService;

    @PostMapping("/login")
    public ResponseEntity<ApiResponseDataDTO> loginCustomers(
            @RequestBody LoginRequest request) {
        return new ResponseEntity<>(loginService.loginCustomers(request), HttpStatus.CREATED);
    }
}

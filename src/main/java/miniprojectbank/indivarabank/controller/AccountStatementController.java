package miniprojectbank.indivarabank.controller;

import com.lowagie.text.DocumentException;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import miniprojectbank.indivarabank.dto.mutation.AccountStatementDTO;
import miniprojectbank.indivarabank.service.AccountStatementService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/account-statements")
public class AccountStatementController {

    private final AccountStatementService accountStatementService;

    @GetMapping("/{accountId}")
    public ResponseEntity<AccountStatementDTO> getAccountStatementByAccountId(
            @RequestHeader("token") String token,
            @PathVariable Long accountId, HttpServletResponse response) {
        try {
            accountStatementService.generatePdfAccountStatementByAccountId(token, accountId, response);

            var headers = new HttpHeaders();
            headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=account-statement.pdf");

            return ResponseEntity.ok().headers(headers).build();
        } catch (IOException | DocumentException e) {
            return ResponseEntity.status(500).build();
        }
    }
}
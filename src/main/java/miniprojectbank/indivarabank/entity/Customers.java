package miniprojectbank.indivarabank.entity;

import jakarta.persistence.*;
import lombok.Data;

import java.util.List;

@Entity
@Data
public class Customers {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long customerId;

    @Column(length = 20)
    private String firstName;

    @Column(length = 20)
    private String lastName;

    @Column(length = 15, unique = true)
    private String phone;

    @Column(length = 20, unique = true)
    private String username;

    private String password;

    private String token;

    @OneToMany(mappedBy = "customer")
    private List<Account> accounts;
}

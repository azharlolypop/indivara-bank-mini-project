package miniprojectbank.indivarabank.entity;

import jakarta.persistence.*;
import lombok.Data;

import java.math.BigDecimal;

@Entity
@Data
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long accountId;

    @Column(length = 6)
    private String pin;

    @Column(length = 9, unique = true)
    private String accountNumber;

    private BigDecimal totalBalance;

    @ManyToOne
    @JoinColumn(name = "customer_id")
    private Customers customer;
}

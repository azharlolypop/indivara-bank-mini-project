package miniprojectbank.indivarabank.advice;

import miniprojectbank.indivarabank.dto.custom.ErrorResponse;
import miniprojectbank.indivarabank.exception.ErrorException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(ErrorException.class)
    public ResponseEntity<ErrorResponse> handleCustomerNotFoundException(ErrorException ex) {
        var errorResponse = ex.toErrorResponse();
        return new ResponseEntity<>(errorResponse, HttpStatus.valueOf(errorResponse.getStatus()));
    }
}
package miniprojectbank.indivarabank;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IndivaraBankApplication {
	public static void main(String[] args) {
		SpringApplication.run(IndivaraBankApplication.class, args);
	}
}
# Account API Spec

## Create Account 

Endpoint : POST /api/accounts

Validate : 
- request not null
- pin must exactly numeric
- deposit =< 50000
- username and

Request Body :

```json
{
    "firstName" : "firstName",
    "lastName" : "lastName",
    "phone" : "phone",
    "username" : "username",
    "password" : "secret",
    "pin" : 123456,
    "depositAmount" : 50000
}
```

Response Body (Success) :

```json
{
    "success": true,
    "data": {
        "accountId": 1,
        "firstName": "first name",
        "lastName": "last name",
        "phone": "123456789",
        "username": "username",
        "accountNumber": "123456789",
        "totalBalance": 50000
    }
}
```

Response Body (Failed) :

```json
{
    "status": "kodeStatus",
    "error": "httpStatus",
    "message": "Message Errors Response"
}
```

## Create Second Account

Endpoint : POST /api/accounts/{customerId}

Request Body :

```json
{
    "firstName" : "fullName",
    "lastName" : "lastName",
    "phone" : "phone",
    "username" : "username",
    "password" : "secret",
    "pin" : 123456,
    "depositAmount" : 50000
}
```

Response Body (Success) :

```json
{
    "success": true,
    "data": {
        "accountId": 1,
        "firstName": "first name",
        "lastName": "last name",
        "phone": "123456789",
        "username": "username",
        "accountNumber": "123456789",
        "totalBalance": 50000
    }
}
```

Response Body (Failed) :

```json
{
    "status": "kodeStatus",
    "error": "httpStatus",
    "message": "Message Errors Response"
}
```

## Login Account

Endpoint : POST /api/auth/login

Request Body :

```json
{
    "username" : "username",
    "password" : "secret"
}
```

Response Body (Success) :


```json
{
    "success" : true,
    "data" : {
      "token": "token generate in jwt token"
    }
}
```

Response Body (Failed, 401) :

```json
{
    "status": "kodeStatus",
    "error": "httpStatus",
    "message": "Message Errors Response"
}
```

## Update Account

Endpoint : PUT /api/accounts/{accountId}

Request Header :

- X-API-TOKEN : Token (Mandatory)

Request Body :

```json
{
    "firstName" : "firstName",
    "lastName" : "lastName",
    "phone" : "phone",
    "username" : "username",
    "password" : "secret",
    "pin" : 123456,
    "depositAmount" : 50000
}
```

Response Body (Success) :

```json
{
    "success": true,
    "data": {
        "accountId": 1,
        "firstName": "firstName",
        "lastName": "lastName",
        "phone": "phone",
        "username": "username",
        "accountNumber": "123456789",
        "totalBalance": 50000
    }
}
```

Response Body (Failed, 401) :

```json
{
    "status": "kodeStatus",
    "error": "httpStatus",
    "message": "Message Errors Response"
}
```

## Get Customer By ID

Endpoint : GET /api/accounts/{accountId}

Request Header :

- X-API-TOKEN : Token (Mandatory)

Response Body (Success) :

```json
{
  "success": true,
  "data": {
      "accountId": 1,
      "firstName": "firstName",
      "lastName": "lastName",
      "phone": "phone",
      "username": "username",
      "accountNumber": "123456789",
      "totalBalance": 50000
  }
}
```

Response Body (Failed) :

```json
{
    "status": "kodeStatus",
    "error": "httpStatus",
    "message": "Message Errors Response"
}
```

## Logout Customer

Endpoint : Delete /api/accounts/{accountId}

Request Header :

- X-API-TOKEN : Token (Mandatory)

Response Body (Success) :

```json
{
  "success" : true,
  "data" : "Success"
}
```

Response Body (Failed, 401) :

```json
{
    "status": "kodeStatus",
    "error": "httpStatus",
    "message": "Message Errors Response"
}
```
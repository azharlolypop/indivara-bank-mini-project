# Withdraw API Spec

## Customer Account Withdraw

Endpoint : POST /api/withdraw/{accountId}

Request Header :

- X-API-TOKEN : Token (Mandatory)

Request Body :

```json
{
    "withdrawAmount" : 1000000,
    "pin" : 123456
}
```

Response Body (Success) :

```json
{
    "success" : true,
    "data" : {
      "withdrawId" : 1,
      "accountId" : 1,
      "withdrawDate" : "new date (JSON Format)",
      "withdrawAmount" : 100000
    }
}
```

Response Body (Failed) :

```json
{
    "status": "kodeStatus",
    "error": "httpStatus",
    "message": "Message Errors Response"
}
```
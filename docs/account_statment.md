# Account Statement API Spec

## Get Account Statments Transaction

Endpoint : GET /api/account-statements/{accountId}

Request Header :

- X-API-TOKEN : Token (Mandatory)

Response Body (Success) :

```json
{
  "accountId": 1,
  "username": "username",
  "historyTransaction": [
    {
      "transactionId": 1,
      "type": "deposit",
      "amount": 1000000.00,
      "transactionDate": "2023-12-15 01:43:56"
    },
    {
      "transactionId": 1,
      "type": "withdraw",
      "amount": 1000000.00,
      "transactionDate": "2023-12-15 01:43:56"
    },
    {
      "transactionId": 1,
      "type": "withdraw",
      "amount": 1000000.00,
      "transactionDate": "2023-12-15 01:43:56"
    }
  ]
}
```

Response Body (Failed) :

```json
{
    "status": "kodeStatus",
    "error": "httpStatus",
    "message": "Message Errors Response"
}
```
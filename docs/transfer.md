# Transfer API Spec

## Customer Account Transfer

Endpoint : POST /api/transfer/{accountId}

Request Header :

- X-API-TOKEN : Token (Mandatory)

Request Body :

```json
{
    "transferAmount" : 100000,
    "pin" : 123456,
    "accountNumberDestination" : 3789217398
}
```

Response Body (Success) :

```json
{
    "success" : true,
    "data" : {
      "transferId": 1,
      "accountId": 1,
      "username": "username",
      "transferDate": "new date (JSON Format)",
      "transferAmount": 1000000000,
      "accountNumberDestination": "accountNumberDestination"
    }
}
```

Response Body (Failed) :

```json
{
    "status": "kodeStatus",
    "error": "httpStatus",
    "message": "Message Errors Response"
}
```
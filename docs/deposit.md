# Deposit API Spec

## Customer Account Deposit

Endpoint : POST /api/deposit/{accountId}

Request Header :

- X-API-TOKEN : Token (Mandatory)

Request Body :

```json
{
    "depositAmount" : 1000000,
    "pin" : 123456
}
```

Response Body (Success) :

```json
{
    "success" : true,
    "data" : {
      "depositId" : 1,
      "accountId" : 1,
      "depositDate" : "new date (JSON Format)",
      "depositAmount" : 100000
    }
}
```

Response Body (Failed) :

```json
{
    "status": "kodeStatus",
    "error": "httpStatus",
    "message": "Message Errors Response"
}
```